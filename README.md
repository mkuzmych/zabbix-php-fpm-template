# Zabbix PHP-FPM template

Zabbix template form monitoring PHP-FPM

### Enable PHP-FPM status page

Change php-fpm configration file and apply the changes.

```bash
$ sudo nano /etc/php-fpm.d/www.conf
pm.status_path = /phpfpm_status
$ sudo systemctl restart php-fpm
```

Nginx configuration for http://{HOST.IP} - change ZABBIX SERVER IP for real:

```bash
server {
    listen       80 default;
    location ~ ^/phpfpm_status {
        access_log off;
        allow {ZABBIX SERVER IP};
        deny all;
        fastcgi_pass unix:/var/run/php-fpm/php-fpm.sock;
        fastcgi_param SCRIPT_FILENAME $request_filename;
        include /etc/nginx/fastcgi_params;
    }
```

```bash
$ sudo systemctl restart nginx
```

Ensure it works.

```bash
$ curl -L 'http://{HOST.IP}/phpfpm_status'
pool:                 www
process manager:      dynamic
start time:           20/Mar/2020:18:01:25 +0200
start since:          384
accepted conn:        282
listen queue:         0
max listen queue:     0
listen queue len:     0
idle processes:       2
active processes:     1
total processes:      3
max active processes: 1
max children reached: 0
slow requests:        0
```

### Setup Zabbix

Request Zabbix UI to configuration the template and link it to hosts.

- Import `Template_app_php_fpm.xml`.  [zabbix doc](https://www.zabbix.com/documentation/current/manual/xml_export_import/templates)
- Link template to hosts.  [zabbix doc](https://www.zabbix.com/documentation/current/manual/config/templates/linking)

### Setup slow log PHP-FPM

```bash
$ sudo nano /etc/php-fpm.d/www.conf
slowlog = /var/log/php-fpm/www-slow.log
request_slowlog_timeout = 10s
$ sudo systemctl restart php-fpm
```

### Done
